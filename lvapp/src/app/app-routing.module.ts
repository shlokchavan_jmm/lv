import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingpageComponent } from './components/landingpage/landingpage.component';


const routes: Routes = [
  {
    path: '', pathMatch:'full', redirectTo:'/'
  },
  {
    path: '', component: LandingpageComponent
   },
   {
     path:'**',component:LandingpageComponent 
   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
